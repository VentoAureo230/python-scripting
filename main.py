import sys
from io import StringIO
from dotenv import load_dotenv
import requests
import smtplib
import os
import socket
from tqdm import tqdm
from http import HTTPStatus


class UrlTester:
    """
    def check_site_is_available(filename):
        urls = readfiles([filename])

        for url in urls:
            url = url.strip()
            headers = {'Authorization': 'token {TOKEN}'}
            r = requests.get(url, stream=True, headers=headers)
            total_length = int(r.headers.get('content-length'))
            for chunk in progress.bar(r.iter_content(chunk_size=1024), expected_size=total_length / 1024) + 1:
                if chunk:
                    url.write(chunk)
                    url.flush()
    """

    def __init__(self):
        pass

    def test_sites(self):
        with open('./websites.txt', 'r') as f:
            total_websites = sum(1 for _ in f)
            f.seek(0)
            for line in tqdm(f, total=total_websites, desc="Parsing file", unit="line"):
                url = line.strip()
                headers = {'Authorization': 'token TOKEN'}
                # with tqdm(desc=f"Pinging {url}", unit="website", leave=False):  --> it displays which urls is being pinged (could just be done in a loop with print)
                try:
                    result = requests.get(url, headers=headers)
                    if result.status_code != HTTPStatus.OK:
                        print(f"Failed to ping {url}")
                        print(result)
                except requests.exceptions.SSLError:
                    print(f"Failed to target {url} - SSLCertVerificationError")


class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self

    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio  # free up some memory
        sys.stdout = self._stdout


def exo_2():
    load_dotenv()

    with Capturing() as output:
        test_sites()

    if len(output):
        server = smtplib.SMTP_SSL(os.getenv('EMAIL_SMTP'), os.getenv('EMAIL_PORT'))
        server.login(os.getenv('EMAIL_USER'), os.getenv('EMAIL_PASS'))
        message = "subject: test \n"

        for line in output:
            message += line + "\n"

        server.sendmail(
            os.getenv('EMAIL_USER'),
            os.getenv('DESTINATION_EMAIL'),
            message)
        server.quit()
        print(f'Message sent to %s' % message)


def exo_3():
    print("EXERCICE 3 : \n")
    with open('websites_and_ports.txt', 'r') as fichier:
        # Lire chaque ligne du fichier
        for ligne in fichier:

            # Diviser la ligne en mots en utilisant l'espace comme séparateur afin de prendre uniquement les ports

            website, port_str = ligne.split()

            # On transforme la chaine de caractère en int

            port = int(port_str)
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            result = sock.connect_ex((website, port))
            if result == 0:
                print("Port :", port, " is open")
            else:
                print("Port :", port, " is not open")
            sock.close()


if __name__ == "__main__":
    # exo_1()
    exo_2()
