# Python scripting
L'objectif de ce dépôt est de s'entraîner à réaliser des scripts python. 

## Exercice 1
A partir du fichier `websites.txt`, implémentez un script permettant de vérifier que tous les sites sont bien up. 
Pour ce faire, vous aurez besoin d'utiliser une bibliothèque extérieure, `requests` par exemple.

# Version complexe :

Lors de l'appel vers les urls, afficher une progress bar qui render la durée de l'appel. Ne pas oublier de gérer le cas de l'échec de l'appel (smtp.google.com). Utiliser un affichage graphique (terminal ou GUI)

## Exercice 2
A partir du script précédent, informez l'administrateur que le site est down. 
Pour ce faire, le script devra envoyer un email à une adresse mail spécifiée dans le fichier `.env`.
Vous aurez probablement besoin d'une bibliothèque tierce, `smtplib` par exemple.

# Version complexe : 

Utiliser une GUI ou le terminal, pour que le user rentre lui même les adresses d'envoi, de reception et le mots de passe

## Exercice 3
Hormis le fait de tester des sites, il est parfois nécessaire de vérifier si des ports sont disponibles.
Updatez le script précédent afin de vérifier les ports, vous avez pour celà le fichier `websites_and_ports.txt`.
Pourquoi ne pas tester `socket` cette fois-ci ?

## Exercice 4
Réalisez un script python permettant de parcourir un dossier. Vous aurez probablement recours à la bibliothèque `os`.
et lister l'ensemble des fichiers doublons présents dans ce dossier (d'une manière récursive).

Le test sur le fait d'être un doublon doit couvrir l'un des critères suivants : 
- un même nommage de fichier
- un fichier ayant le même contenu

L'ensemble des fichiers doublons doit être retourné, par exemple avec le dossier `data` ci -joint voitre script doit retourner : 
```
Fichier ayant ayant le même nom
- data/premier_doublon.txt
- data/logs/premier_doublon.txt
Fichier ayant ayant le même contenu
- data/second_d.ini
- data/subfolder/subfolder/second_doublon.ini
```

## Exercice 5
Réalisez un script python permettant de lister la taille des fichier par ordre décroissant. 
Votre script ne doit afficher que les fichiers ayant une taille supérieure à 40 octets.
Par ordre décroissant au niveau taille de fichier. 
