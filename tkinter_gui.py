import tkinter
import tkinter as tk
from tkinter import ttk, filedialog as fd, Button
from main import UrlTester


class TkGui:
    def __init__(self):
        self.root = tk.Tk()
        self.root.title('URLs Tester')
        self.root.geometry('700x800')
        self.root.grid()

        self.create_text_boxes('websites.txt')
        self.create_loadingbar()
        self.select_file()

        self.root.mainloop()

    def create_text_boxes(self, file_path):
        if self.select_file():
            with open(file_path, 'r') as file:
                lines = file.readlines()

            for line in lines:
                x = 0
                text_box = tk.Text(self.root, height=1, width=80)
                text_box.insert(tk.END, line.strip())
                text_box.grid(column=1, padx=10, pady=10)
    
    def create_loadingbar(self):
        # Progress bar
        self.pb = ttk.Progressbar(
            self.root,
            orient='horizontal',
            mode='indeterminate',
            length=280
        )
        self.pb.grid(
            column=0,
            row=5,
            columnspan=2,
            padx=10,
            pady=20,
            sticky=tkinter.S
        )
        self.btn_confirm = Button(self.root, text='Test URLs', command=UrlTester.test_sites)
        self.btn_confirm.grid(column=1, row=6, padx=10, pady=10, sticky=tk.S)

    def select_file(self):
        filetype = (
            ('Text files', '*.txt'),
            ('All files', '*.*')
        )
        self.filename = fd.askopenfile(
            title="Open a file",
            initialdir='/',
            filetypes=filetype
        )
        self.open_btn = Button(
            self.root,
            text="Open a file",
            command=self.select_file
        )
        self.open_btn.grid(column=1, row=7, padx=10, pady=10)


# Create an instance of the TkGui class
app = TkGui()
